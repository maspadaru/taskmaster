import taskmaster
from taskmaster import Master, Worker
import os

class HWMaster(Master):
    def create_tasks(self, input_file):
        '''
        input_file: one of the files in the IN_DIR open for reading

        call self.queue_task(task) to send tasks to workers
        '''
        pass

    def collect(self, result):
        '''
        result: Object - result returned by worker method Worker.work(task)

        return String - this string will be written to the output
            file OUT_DIR/out.data
        '''
        pass

class HWWorker(Worker):
    def work(self, task):
        '''
        task: one of the tasks created by Master.create_tasks(input_file)

        return Object - will be sent back to master and processed
            in Master.collect(result)
        '''
        pass


def run():
    cofig_file = os.getcwd()+"/taskmaster.conf"
    master = HWMaster();
    worker = HWWorker();
    taskmaster.run(master, worker, cofig_file)

if __name__ == '__main__':
    run()
