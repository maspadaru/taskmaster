import taskmaster
from taskmaster import Master, Worker
import os
import nltk
import re
from datetime import datetime

class HWMaster(Master):

    def create_tasks(self, f):
        '''
        Creates tasks of about 10000 tweets.
        Checks date so to keep tweets from the same day grouped in one task
        '''
        reset = False
        count = 0
        tweet_list = []
        last_date = None
        for line in f:
           if line.startswith('T'):
               date = re.search('T\w*(\s+)(.*)(\n)', line).group(2)
               tweet_date = datetime.strptime(date, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
               if reset and tweet_date == last_date:
                   count = 0
                   self.queue_task(tweet_list)
                   tweet_list = []
                   reset = False
               tweet = line
           if line.startswith("U"):
               tweet = tweet + line
           if line.startswith("W"):
               if line == "No Post Title":
                   line =""
               else:
                   tweet = tweet + line
                   tweet_list.append(tweet)
                   del line
                   del tweet
           count += 1
           if count >= 10000:
               reset = True
               last_date = tweet_date
        self.queue_task(tweet_list)



    def collect(self, result):
        results = {}
        for tweet in result:
            date = tweet[0]
            is_positive = tweet[1] == "POSITIVE"
            is_date = date in results
            pos = results[date][0] if is_date else 0
            neg = results[date][1] if is_date else 0
            if is_positive:
                pos += 1
            else:
                neg += 1
            results[date] = (pos, neg)
        result = ""
        for date, tup in results.iteritems():
            result = result + '"{0}" {1} {2}\n'.format(date, tup[0], tup[1])
        return result


class HWWorker(Worker):

    def check(self, tweet, classifier):
        words = tweet.split()
        feats = dict([(word, True) for word in words])
        return classifier.classify(feats) == 'pos'

    def process_message(self, message, classifier):
        m = re.search('W(\s+)(.*)(\n)', message, re.S)
        if m:
            tw = m.group(2)
            date = re.search('T\w*(\s+)(.*)(\n)', message).group(2)
            tweet_date = datetime.strptime(date, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
            if self.check(tw, classifier):
                return (tweet_date, "POSITIVE")
            else:
                return (tweet_date, "NEGATIVE")

    def work(self, message):
        result = []
        cwd = os.getcwd()
        pickle_name = "/movie_reviews_NaiveBayes.pickle"
        file_name =cwd + pickle_name
        classifier = nltk.data.load("file:"+file_name)
        for tweet in message:
            result.append(self.process_message(tweet, classifier))
        return result


def run():
    cofig_file = os.getcwd()+"/taskmaster.conf"
    master = HWMaster();
    worker = HWWorker();
    taskmaster.run(master, worker, cofig_file)

if __name__ == '__main__':
    run()
