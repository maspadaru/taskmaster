import taskmaster
from taskmaster import Master, Worker
import os

class HWMaster(Master):
    def create_tasks(self, file):
        task_list = []
        for index in range(1, 1000):
            task_list.append("{0}".format(index))
            if index % 100 == 0:
                self.queue_task(task_list)
                task_list = []
        self.queue_task(task_list)

    def collect(self, result_list):
        collected = ""
        for result in result_list:
            collected += result
        return collected

class HWWorker(Worker):
    def work(self, messages):
        result = []
        for message in messages:
            res = "Worker {0}, recevied message {1}\n" \
                .format(self.get_id(), message)
            result.append(res)
        return result


def run():
    cofig_file = os.getcwd()+"/taskmaster.conf"
    master = HWMaster();
    worker = HWWorker();
    taskmaster.run(master, worker, cofig_file)

if __name__ == '__main__':
    run()
