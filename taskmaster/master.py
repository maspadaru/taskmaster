from collections import deque
import requests
from threading import Thread
import json
from os import listdir
from time import sleep
from os.path import isfile, join
import broadcaster

WORKER_IDLE = 0
WORKER_BUSY = 1
WORKER_TIMEOUT = 2
PORT = "5000"

class Worker():
    def __init__(self, uuid, ip, status):
        self.uuid = uuid
        self.ip = ip
        self.status = status

class Master():
    def __init__(self):
        self.result_queue = deque()
        self.worker_pool = {}
        self.thread_count = 0
        self.in_path = ""
        self.ou_path = ""
        self.task_queue = deque()
        self.is_read_done = False
        self.is_working = False
        self.write_lock = False
        self.done = False

    def create_tasks(self, input_file):
        pass

    def collect(self, result):
        pass

    def queue_task(self, task):
        self.task_queue.append(task)
        while (len(self.task_queue) > 4 * len(self.worker_pool)):
            sleep(0.5)

    def _collect(self, result):
        # TODO propper lock!
        while(self.write_lock):
            sleep(5)
        self.write_lock = True
        try:
            cwd = self.out_path
            output_file = open(cwd + "/out.data", 'a')
            collected = self.collect(result)
            output_file.write(collected)
        finally:
            self.write_lock = False

    def _are_workers_busy(self):
        for worker in self.worker_pool.values():
            if worker.status == WORKER_BUSY:
                return True
        return False


    def _create_tasks(self):
        self.is_read_done = False
        cwd = self.in_path + "/"
        onlyfiles = [ f for f in listdir(cwd) if isfile(join(cwd,f)) ]
        for fl in onlyfiles:
            fullpath = cwd+fl
            with open(fullpath) as f:
                self.create_tasks(f)
        self.is_read_done = True


    def _start(self):
        out = open(self.out_path + "/out.data", 'w')
        if out:
            out.close() #clearing output files
        Thread(target=self._discover).start()
        Thread(target=self._dispatch).start()
        Thread(target=self._create_tasks).start()
        while not self.done:
            sleep (5)
            self.done = self.is_read_done and not self._are_workers_busy() \
                and len(self.task_queue) == 0
        print "done!"

    def _discover(self):
        while not self.done:
            uuid, addr = broadcaster.listen(self._task)
            if uuid not in self.worker_pool:
                worker = Worker(uuid, addr, WORKER_IDLE)
                self.worker_pool[uuid] = worker
                print "worker connected, uuid = ", uuid, "ip = ", addr


    def _dispatch(self):
        work = True
        while (work or not self.done):
            work = len(self.task_queue) > 0
            for worker in self.worker_pool.values():
                if worker.status == WORKER_IDLE:
                    if work:
                        task = self.task_queue.pop()
                        worker.status = WORKER_BUSY
                        Thread(target=self._send_task, \
                            args=(worker.uuid, task)).start()
                elif worker.status == WORKER_BUSY:
                    work = True
                # if len(self.task_queue) == 0:
                #    work = False

    def _send_task(self, worker_uuid, task):
        dest = "http://" + self.worker_pool[worker_uuid].ip + ":" + PORT + "/task"
        try :
            r = requests.post(dest, data={'task': json.dumps(task)})
        except requests.ConnectionError:
            r = None
        if r and r.status_code == 200:
             status = WORKER_IDLE
             self._receive_result(r.text)
        else:
            status = WORKER_TIMEOUT
            self.task_queue.append(task)
        self.worker_pool[worker_uuid].status = status

    def _receive_result(self, text):
        result = json.loads(text)
        self._collect(result)
