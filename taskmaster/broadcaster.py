from time import sleep
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST

PORT = 50000


def broadcast(key, uuid):
    s = socket(AF_INET, SOCK_DGRAM)
    s.bind(('', 0))
    s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    msg = key + ";" + uuid
    s.sendto(msg, ('<broadcast>', PORT))

def listen(key):
    s = socket(AF_INET, SOCK_DGRAM)
    s.bind(('', PORT))
    while True:
        data, addr = s.recvfrom(1024) # wait for a packet
        if data.startswith(key):
            uuid = data.split(";")[1]
            return uuid, addr[0]
