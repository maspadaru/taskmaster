from configparser import ConfigParser
import worker_backend
import uuid

def run(master, worker, config_file):
    job, task, master_config = read_settings(config_file)
    if job == "MASTER":
        run_master(master, task, master_config)
    else:
        run_worker(worker, task)


def read_settings(config_file):
    config = ConfigParser()
    config.read(config_file)
    job = config['main']['job']
    task = config['main']['task']
    master = config['master']
    return job, task, master


def run_master(master, task, master_config):
    # configure
    print("Running master")
    master._task = task
    master.in_path = master_config["IN_DIR"]
    master.out_path = master_config["OUT_DIR"]
    master._start()

def run_worker(worker, task):
    worker._task = task
    worker._id = str(uuid.uuid4())
    worker._start()
    worker_backend.worker = worker
    worker_backend.app.run(host='0.0.0.0')
