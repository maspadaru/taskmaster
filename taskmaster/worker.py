import broadcaster
from threading import Thread
from time import sleep

class Worker():
    def __init__(self):
        self._id = ""
        self._task = ""
        self._has_work = False

    def work(self, task):
        pass

    def get_id(self):
        return self._id

    def _start(self):
        Thread(target=self._discover).start()

    def _discover(self):
        while True:
            if not self._has_work:
                broadcaster.broadcast(self._task, self._id)
            sleep(20)
