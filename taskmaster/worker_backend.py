from flask import Flask, request
from flask_restful import Resource, Api, reqparse
import json

app = Flask(__name__)
api = Api(app)
worker = None
parser = reqparse.RequestParser()
parser.add_argument('task')

class WorkerTask(Resource):
    def post(self):
        args = parser.parse_args()
        worker._has_work = True
        task = args['task']
        result = worker.work(json.loads(task))
        worker._has_work = False
        return result, 200

api.add_resource(WorkerTask, '/task')
