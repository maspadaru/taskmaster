# Taskmaster

Taskmaster is a light-weight open-source software framework that aims to simplify distribution of big data processing and analysis tasks over multiple worker nodes.   

## How to use
Copy the taskmaster folder and skeleton.py and taskmaster.comf.template in your source folder.
Edit skeleton.py, implementing the methods: create_tasks, collect and work.
You need to copy your code on the master machine and on all the nodes.
Edit taskmaster.conf.template and rename it to taskmaster.conf on each node.
Run your program o each node.

Check out the examples. You can find instructions on running the examples in each example's folder. 

### Edit taskmaster.conf 
On the Master Node, it is importat to specify the input and output directories (IN_DIR and OUT_DIR). The Job parameter must be set to MASTER.
On the worker nodes this information may be skipped, but it is important to set the Job as WORKER.

## How it works
The framework uses one Master Node that will dispatch tasks to multiple Workers. All the files in the IN_DIR folder are read one at a time. The programmer can parse each file by implementing the method Master.create_task(input_file). The programmer can itterate through the data in this file and crate tasks by calling the method Master.queue_task(task). In an effort to prevent overloading of the system's memory, the creation of the tasks is handled by a different thread. This thread that will stop loading data to memory if the size of the tasks queue excedes by four times the number of workers. When some of the tasks have been handled, the parsing of new data from the input files and creating of new tasks, can resume. 

Each taks is loaded into a queue. A dispacher thread attempts to pop one task from the queue and dispatch it to an idle worker form the worker pool. A new thread is created that will handle the delivery of the task and also handle the result. The tasks are delivered using HTTP POST requests. The objects are encoded using JSON. In case of a Worker not responding to the HTTP request, the task is pushed back in the queue. When receiving a task by the Worker, the method Worker.Work(task) will be called. This method need to be overridden by the programmer in a class that extends Worker. 

When a result is received from a worker, the method Master.Collect(result) is invoked. This method has to be overriden by the programmer. After the result is parsed, the method should return a string that will be written to the output file in the folder OUT_DIR. 

Workers can be added at runtime. Each worker will broadcast it's ID. When receiving such a broadcast, the master will add the worker to it's worker pool. 

# Dependencies
```
pip install configparser requests flask flask-restful
```



